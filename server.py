import logging
import eventlet
import socketio


sio = socketio.Server()
app = socketio.WSGIApp(sio, static_files={
    '/': {'content_type': 'text/html', 'filename': 'index.html'}
})

@sio.event
def connect(sid, environ):
    logging.info('connect', sid)

@sio.event
def register(sid, name):
    sio.emit('join', name)
    members[sid] = name
    sio.emit('mlist', members)

@sio.event
def message(sid, msg):
    sio.emit('message', f'{members[sid]}: {msg}')

@sio.event
def disconnect(sid):
    logging.info('disconnect', sid)
    del members[sid]
    sio.emit('mlist', members)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    members = {}
    eventlet.wsgi.server(eventlet.listen(('', 5000)), app)
