import logging
import socketio


sio = socketio.Client()

@sio.event
def connect():
    logging.info('connect')

@sio.event
def join(name):
    print(f'{name} 悄悄地加入聊天室了...', end='\n$ ')

@sio.event
def mlist(members):
    logging.info(members)    

@sio.event
def message(msg):
    # print(msg, end='\n$ ')
    pass

@sio.event
def disconnect():
    logging.info('disconnect')

def sendMsg(msg):
    sio.emit('message', msg)

def register(name):
    sio.connect('//localhost:5000')
    sio.emit('register', name)


if __name__ == '__main__':
    # logging.basicConfig(level=logging.INFO)
    register(input('Nickname: '))
    while True:
        user_input = input('$ ')
        sendMsg(user_input)
