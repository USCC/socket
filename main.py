import sys
import logging
import socketio
from PyQt5 import QtWidgets
from Ui_main import Ui_MainWindow as Ui_main
from Ui_nickname import Ui_Dialog as Ui_nickname


sio = socketio.Client()

@sio.event
def connect():
    logging.info('connect')

@sio.event
def join(name):
    ui.textBrowser.append(f'{name} 悄悄地加入聊天室了...')

@sio.event
def mlist(members):
    logging.info(members)    

@sio.event
def message(msg):
    # ui.textBrowser.append(msg)
    ui.textBrowser.ensureCursorVisible()

@sio.event
def disconnect():
    logging.info('disconnect')

def sendMsg():
    sio.emit('message', ui.plainTextEdit.toPlainText())
    ui.plainTextEdit.clear()

def register():
    sio.connect('//localhost:5000')
    sio.emit('register', ui_nickname.lineEdit.text())
    Dialog.close()


if __name__ == '__main__':
    # logging.basicConfig(level=logging.INFO)
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_main()
    ui.setupUi(MainWindow)
    ui.pushButton.clicked.connect(sendMsg)

    Dialog = QtWidgets.QDialog()
    ui_nickname = Ui_nickname()
    ui_nickname.setupUi(Dialog)
    ui_nickname.pushButton.clicked.connect(register)

    MainWindow.show()
    Dialog.show()

    sys.exit(app.exec_())
